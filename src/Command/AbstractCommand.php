<?php

namespace Kudze\LumenBaseCli\Command;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

abstract class AbstractCommand extends Command
{
    protected SymfonyStyle $io;

    protected function initialize(InputInterface $input, OutputInterface $output): void
    {
        $this->io = new SymfonyStyle($input, $output);
    }

    public function handle(): int
    {
        $this->io->title($this->makeTitle());
        return self::SUCCESS;
    }

    protected function makeTitle(): string
    {
        return "Running " . $this->getName() . "...";
    }
}
